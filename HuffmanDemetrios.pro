#-------------------------------------------------
#
# Project created by QtCreator 2015-05-11T10:42:58
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = HuffmanDemetrios
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    huffman.cpp

HEADERS += \
    huffman.h
