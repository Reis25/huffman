#include "huffman.h"
#include <QIODevice>
#include <QFile>
#include <QDebug>
#include <QByteArray>


int * Huffman::countByte(QString diretorio){
    //256 é escolhido pelo número de caracteres que dispõe a tabela ASC
    int frequencia[256]={0};
    QFile *file = new QFile(diretorio);
    //Leitura do arquivo, apenas.
    if(file->open(QIODevice::ReadOnly)){
        //Guardando o arquivo em um array.
        QByteArray array = file->readAll();
        for(int i = 0; i < array.size(); ++i){
            // Array.at(i) faz com que o array seja preenchido de acordo com o seu devido código da tabela ASC.
            frequencia[uchar(array.at(i))]++;
        }
    }
    return frequencia;
}
